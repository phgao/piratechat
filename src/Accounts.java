import java.io.File;
import java.io.IOException;

import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.activemq.broker.BrokerService;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

public class Accounts {
   private String user, password, group;
   public BrokerService broker;
   
   public Accounts(String user, String password, String group){
      this.user = user;
      this.password = password;
      this.group = group;
   }
   
   
   /**
    * Uses member variables to create new account by accessing the activemq xml file,
    * parsing it, and add a node with the account.
    * @throws Exception
    */
   public void newAccount() throws Exception{
      String filepath = "c:\\ActiveMQ\\apache-activemq-5.9.1\\conf\\activemq.xml";
      try {
         // Getting accessor the the xml file
         DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
         DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
         Document doc = docBuilder.parse(filepath);
  
         // Get the user element by tag name directly
         Node users = doc.getElementsByTagName("users").item(0);
  
         // append a new node to users element
         Element authuser = doc.createElement("authenticationUser");
         authuser.setAttribute("username", user);
         authuser.setAttribute("password", password);
         authuser.setAttribute("groups", group);
         users.appendChild(authuser);
  
         // write the content into xml file
         TransformerFactory transformerFactory = TransformerFactory.newInstance();
         Transformer transformer = transformerFactory.newTransformer();
         DOMSource source = new DOMSource(doc);
         StreamResult result = new StreamResult(new File(filepath));
         transformer.transform(source, result);
  
         System.out.println("Account Added");
         
        } catch (ParserConfigurationException pce) {
         pce.printStackTrace();
        } catch (TransformerException tfe) {
         tfe.printStackTrace();
        } catch (IOException ioe) {
         ioe.printStackTrace();
        } catch (SAXException sae) {
         sae.printStackTrace();
        }
      
      broker = new BrokerService();
      // configure the broker
      broker.setPersistent(false);
      
      broker.addConnector("tcp://localhost:61616");
      broker.start();
   }
}
