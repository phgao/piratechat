import static org.junit.Assert.*;
import static org.easymock.EasyMock.*;

import java.awt.Frame;

import org.junit.Before;
import org.junit.Test;

public class LoginDialogTest {

	private LoginDialog login;
	private Frame f;
	
	@Before
	public void setUp(){
		f = new Frame(); 
		login = new LoginDialog(f);
	}
	
	@Test
	public void testLoginDialog() { 
		assertEquals(login.getOwner(), f); 
		assertEquals(login.getTitle(), "Simple Chat: Connect information");
		assertEquals(login.isResizable(), false);
		assertEquals(login.isModal(), true); 
	}

	@Test
	public void testClear() {
		login.clear();
		assertEquals(login.getUserName(), "");
		assertEquals(login.getPassword(), "");
	}

	@Test
	public void testGetUserName() {
		// probably need mock objects 
	}

	@Test
	public void testGetPassword() {
		// probably need mock objects 
	}

}
