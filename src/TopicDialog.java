import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.GridBagLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextField;


public class TopicDialog extends Dialog{
   /**
    * 
    */
   private static final long serialVersionUID = 1L;

   private TextField topicF;
   private Button joinB;

   /**
    * LoginDialog constructor.
    * 
    * @param f
    *           Parent frame.
    */
   public TopicDialog(Frame f) {
      super(f, "Topic", true);
      init();
      setResizable(false);
   }

   /**
    * Return 'Login' button
    */
   public Button getJoinButton() {
      return (joinB);
   }
   
   /**
    * Clear textfields
    */
   public void clear(){
      topicF.setText("");
   }

   /**
    * Return chat user name entered.
    */
   public String getTopic() {
      if (topicF == null)
         return (null);
      return (topicF.getText());
   }

   /*
    * Init GUI elements.
    */
   private void init() {

      Label userLabel, passwordLabel;
      
      Panel dummyPanel;

      setLayout(new BorderLayout(0, 0));

      userLabel = new Label("Topic");

      dummyPanel = new Panel();
      dummyPanel.setLayout(new BorderLayout(0, 0));
      topicF = new TextField(40);
      topicF.setEditable(true);

      dummyPanel.add("North", userLabel);
      dummyPanel.add("South", topicF);
      add("North", dummyPanel);
      
      dummyPanel = new Panel();
      dummyPanel.setLayout(new GridBagLayout());
      joinB = new Button("Join");
      dummyPanel.add(joinB);
      add("South", dummyPanel);

      pack();
   }

}
