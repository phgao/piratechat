import static org.junit.Assert.*;

import javax.jms.Message;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class ChatBaseTest {
	
	ChatBase jack;
	ChatBase will;
	String [] jArgs = {"tcp://localhost:61616", "JackSparrow", "BlackPearl"}; 
	String [] wArgs = {"tcp://localhost:61616", "WillTurner", "BlackPearl"};
	
	
	@Before
	public void setUp() {
		jack = new ChatBase(); 
		will = new ChatBase();
		jack.url = "tcp://localhost:61616";
		will.url = "tcp://localhost:61616";
		jack.topicName = "BlackPearl";
		will.topicName = "BlackPearl";
	    jack.name = "Janarthan";
	    will.name = "Janarthan";
	    jack.password = "password";
	    will.password = "password";
		
		jack.initializeActiveMq();
		will.initializeActiveMq();
	}

	@Test
	public void testInitializeActiveMq() {
		String jURL = jack.connectionFactory.getBrokerURL(); 
		String wURL = will.connectionFactory.getBrokerURL();
		
		boolean correctURL = jURL.equals("tcp://localhost:61616");
		assertTrue(correctURL);
		
		correctURL = wURL.equals("tcp://localhost:61616");
		assertTrue(correctURL);
	}

	@Test
	public void testDoConnect() {
		jack.doConnect();
		will.doConnect();
		
		assertTrue(jack.connectedToChatSession());
		assertTrue(will.connectedToChatSession()); 
	}
	
	@Test
	public void testSendMessage() {
		try{ 
			// send message
			Message msg = jack.outgoingMsgCreator.createChatMessage(jack.session, 
					jack.name, ChatMessageTypes.NORMAL, 
					"This is the day you will always remember as the day you " +
					"almost caught Captain Jack Sparrow");

			jack.msgProducer.send(msg);
			
			// receive message
			ChatMessageCreator inboundMsgCreator = will.getMessageCreator();
			
			if(inboundMsgCreator == null) {
				fail("Message recieved not supported!");
			}
			
			int type = inboundMsgCreator.getChatMessageType(msg);

		    if (type == ChatMessageTypes.BADTYPE) {
		         fail("Message received in wrong format!");
		         return;
		      }
		}
		catch(Exception ex) {
			fail("Caught unexpected exception while trying to send message");
		}		
	}

	@Test
	public void testDoDisconnect() {
		jack.doDisconnect();
		will.doDisconnect();
		
		assertFalse(jack.connectedToChatSession());
		assertFalse(will.connectedToChatSession());
	}

	@After
	public void tearDown() {
		
	}
}