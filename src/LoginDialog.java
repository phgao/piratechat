import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Choice;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextField;

class LoginDialog extends Dialog {

   /**
    * 
    */
   private static final long serialVersionUID = 1L;

   private TextField nameF, passwordF;
   private Button loginB, createB;

   /**
    * LoginDialog constructor.
    * 
    * @param f
    *           Parent frame.
    */
   public LoginDialog(Frame f) {
      super(f, "Login", true);
      init();
      setResizable(false);
   }

   /**
    * Return 'Login' button
    */
   public Button getLoginButton() {
      return (loginB);
   }
   
   /**
    * Return 'CreateAccount' button
    */
   public Button getCreateButton() {
      return (createB);
   }
   
   /**
    * Clear textfields
    */
   public void clear(){
      nameF.setText("");
      passwordF.setText("");
   }

   /**
    * Return chat user name entered.
    */
   public String getUserName() {
      if (nameF == null)
         return (null);
      return (nameF.getText());
   }

   /**
    * Get password.
    * 
    * @param s
    */
   public String getPassword() {
      if (passwordF == null)
         return (null);
      return (passwordF.getText());
   }

   /*
    * Init GUI elements.
    */
   private void init() {

      Label userLabel, passwordLabel;
      
      Panel dummyPanel;

      setLayout(new BorderLayout(0, 0));

      userLabel = new Label("Username");

      dummyPanel = new Panel();
      dummyPanel.setLayout(new BorderLayout(0, 0));
      nameF = new TextField(40);
      nameF.setEditable(true);

      dummyPanel.add("North", userLabel);
      dummyPanel.add("South", nameF);
      add("North", dummyPanel);

      dummyPanel = new Panel();
      dummyPanel.setLayout(new BorderLayout(0, 0));
      passwordLabel = new Label("Password");
      passwordF = new TextField(40);
      passwordF.setEchoChar('*');
      dummyPanel.add("North", passwordLabel);
      dummyPanel.add("South", passwordF);
      add("Center", dummyPanel);
      
      dummyPanel = new Panel();
      dummyPanel.setLayout(new GridBagLayout());
      loginB = new Button("   ---Login---   ");
      createB = new Button("CreateAccount");
      dummyPanel.add(loginB);
      dummyPanel.add(createB);
      add("South", dummyPanel);
      
      pack();
   }

}
