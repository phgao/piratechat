import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import javax.jms.*;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;

public class ChatTextMessageCreatorTest {
	ChatMessageCreator cmc;
	Connection con;
	Session sesh;
	Message msg;
	String sender, type, str;
	Destination destination;
	String MSG_SENDER_PROPNAME = "SIMPLECHAT_MSG_SENDER";
	String MSG_TYPE_PROPNAME = "SIMPLECHAT_MSG_TYPE";

	@Before
	public void setUp() throws JMSException {
		sender = "Bob";
		type = "type";
		str = "This is the message.";

		String url = ActiveMQConnection.DEFAULT_BROKER_URL;
		ConnectionFactory connectionFactory = new ActiveMQConnectionFactory("Janarthan", "password", url);
		con = connectionFactory.createConnection();
		con.start();
		sesh = con.createSession(false, Session.AUTO_ACKNOWLEDGE);
	}

	@Test
	public void testCreateChatMessage() throws JMSException {
		TextMessage tm = null;
		cmc = new ChatTextMessageCreator();
		tm = (TextMessage) cmc.createChatMessage(sesh, sender, 1, str);
		assertEquals(tm.getStringProperty(MSG_SENDER_PROPNAME), "Bob");
 		assertEquals(tm.getIntProperty(MSG_TYPE_PROPNAME), 1);
		assertEquals(tm.getText(), str);
		assertFalse(tm == null);
	}

	@Test
	public void testIsUsable() throws JMSException {
		Message good_msg1 = null;
		cmc = new ChatTextMessageCreator();
		good_msg1 = (Message) sesh.createTextMessage("howdy");
		assertTrue(cmc.isUsable(good_msg1));

		Message good_msg2 = (Message) sesh.createTextMessage();
		assertTrue(cmc.isUsable(good_msg2));

		ObjectMessage bad_msg = sesh.createObjectMessage("yoooooo");
		assertFalse(cmc.isUsable(bad_msg));
	}

	@Test
	public void testGetChatMessageType() throws JMSException {
		TextMessage tm;
		cmc = new ChatTextMessageCreator();
		tm = (TextMessage) cmc.createChatMessage(sesh, sender, 1, str);
		assertEquals(tm.getIntProperty(MSG_TYPE_PROPNAME), cmc.getChatMessageType(tm));
		tm.setIntProperty(MSG_TYPE_PROPNAME, 0);
		assertTrue(cmc.getChatMessageType(tm) == tm.getIntProperty(MSG_TYPE_PROPNAME));
	}

	@Test
	public void testGetChatMessageSender() throws JMSException {
	    cmc = new ChatTextMessageCreator();
		msg = cmc.createChatMessage(sesh, sender, 1, str);
		String msgSender = msg.getStringProperty(MSG_SENDER_PROPNAME);
		String cmcSender = cmc.getChatMessageSender(msg);
		assertEquals(msgSender, cmcSender);
		sender = null;
		assertFalse(cmc.getChatMessageSender(msg).equals(sender));
	}

	@Test
	public void testGetChatMessageText() throws JMSException {
		TextMessage tm;
		cmc = new ChatTextMessageCreator();
		tm = (TextMessage) cmc.createChatMessage(sesh, sender, 1, str);
		assertEquals(tm.getText(), cmc.getChatMessageText(tm));
		str = null;
		assertFalse(cmc.getChatMessageText(tm).equals(str));
	}

}