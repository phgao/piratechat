import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;

import javax.jms.*;

import org.apache.activemq.ActiveMQConnectionFactory;

public class ConsoleChat extends ChatBase implements MessageListener {

   boolean connected = false;
   BufferedReader commandLine;
   String inputText = "";

   /**
    * Chat constructor. Initializes the chat user name, topic, hostname.
    */
   public ConsoleChat() {
      super();
      commandLine = new java.io.BufferedReader(new InputStreamReader(System.in));
   }
   
   /*
    * To be overridden by derived classes to output senders message.
    */
   @Override
   protected void outputMessage (String sender, int type, String msgText) {
      System.out.println(sender + ": " + msgText + "\n");
   }
   
   @Override
   protected void initializeActiveMq() {
      super.initializeActiveMq();
      
      String msgText = "";
      try {
         msgText = commandLine.readLine();
      } catch (IOException e) {
         e.printStackTrace();
      }
      
      //loop through till we read an exit.
      while(msgText.toLowerCase() != "exit" ){
         
         //update inputText as it would be used to sendNormalMessage..
         inputText = msgText;
         
         //Now send the normal message.
         sendNormalMessage();
         
         //read the next input line.
         try {
            msgText = commandLine.readLine();
            if (msgText.equalsIgnoreCase("exit")) {
               //System.out.println("Bye!");
               break;
            }
         } catch (IOException e) {
            e.printStackTrace();
         }
      }
      
      //out of the loop close and shutdown.
      try {
         commandLine.close();
      } catch (IOException e) {
         e.printStackTrace();
      }
      commandLine = null;
      exit();
      
   }
   
   private void loginAtConsole(){
      String usrname = "";
      String pssword = "";
      String group = "user";
      String loginType = "";
      while ( usrname == "" || pssword == "" || loginType == ""){
         if( usrname == "" ){
            System.out.println("Enter user name: ");
            try {
               usrname = commandLine.readLine();            
            } catch (IOException e) {}
         }
         if( usrname != "" ){
            System.out.println("Enter password: ");
            try {
               pssword = commandLine.readLine(); 
            } catch (IOException e) {}
         }
         if( pssword != "" ){
            System.out.println("Login or Create Account");
            try{
               loginType = commandLine.readLine();
            } catch(IOException e){}
         }
      }
      
      if( loginType.equals("Create")){
         Accounts account;
         if( usrname != "" && pssword != "" && group != ""){
            account = new Accounts(usrname, pssword, group);   
            try {
               account.newAccount();
               name = usrname;
               password = pssword;
               joinTopic();
            } catch (Exception e) {
               e.printStackTrace();
            }
         }
      }
      if( loginType.equals("Login")){ 
         name = usrname;
         password = pssword;
         joinTopic();
      }
   }
   
   /**
    * Use user input to initialize or join a chat topic
    */
   private void joinTopic(){
      System.out.println("Enter topic to join");
      String topic = "";
      while( topic == "" ){
         try {
            topic = commandLine.readLine();
         } catch (IOException e) {
            e.printStackTrace();
         }
      }
      
      topicName = topic;
      initializeActiveMq();
   }
   
   /**
    * Reads input from the console, and creates a message from it
    * If input is exit, the console is closed and exit is called
    */
   @Override
   protected String fetchInputMessage() throws Exception
   {
      return inputText;
   }

   @Override
   protected void failLogin(){
      System.out.println("Login Failed");
      loginAtConsole();
   }
   
   /**
    * @param args
    *           Arguments passed via the command line. These are used to create
    *           the ConnectionFactory.
    */
   public static void main(String[] args) {
      ConsoleChat sc = new ConsoleChat();

      sc.url = args[0];
      //sc.topicName = args[1];
      //sc.name = args[2];
      //sc.password = args[3];      
      sc.loginAtConsole();
   }

}