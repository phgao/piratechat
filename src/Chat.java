import java.awt.Button;
import java.awt.Frame;
import java.awt.Menu;
import java.awt.MenuBar;
import java.awt.MenuItem;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JOptionPane;

import org.apache.activemq.broker.BrokerService;

public class Chat extends ChatBase implements ActionListener, WindowListener {

   boolean connected = false;

   Frame frame;
   ChatPanel scp;
   LoginDialog loginDlg = null;
   TopicDialog topicDlg = null;
   MenuItem clearItem, exitItem; 
   Button sendB, loginB, createB, joinB;

   /**
    * Chat constructor. Initializes the chat user name, topic, hostname.
    */
   public Chat() {
      super();
   }

   public void actionPerformed(ActionEvent e) {
      Object obj = e.getSource();

      if (obj == sendB) {
         sendNormalMessage();
      } else if (obj == clearItem) {
         scp.clear();
      } else if (obj == exitItem) {
         exit();
      } else if( obj == loginB){
         //proccessLogin();
         //loginDlg.setVisible(false);
         showTopic();
      } else if( obj == createB){
         newAccountInfo();
      } else if( obj == joinB){
         topicGrabber();
         topicDlg.setVisible(false);
         proccessLogin();
      }
   }
   
   /**
    * Grabs account info from UI and initializes the topic
    */
   private void topicGrabber(){
      topicName = topicDlg.getTopic();
   }

   /**
    * Grabs account info from UI and sends it to accounts class
    */
   private void newAccountInfo() {
      Accounts account;
      String user = loginDlg.getUserName();
      String pssword = loginDlg.getPassword();
      String group = "users";
      
      if( user != "" && pssword != "" && group != ""){
         account = new Accounts(user, pssword, group);   
         try {
            account.newAccount();
         } catch (Exception e) {
            e.printStackTrace();
         }
      }
   }

   private void proccessLogin() {
      String usrname = loginDlg.getUserName();
      String pssword = loginDlg.getPassword();
      if( usrname.length() == 0 || pssword.length() == 0){
         return;
      }
      name = usrname;
      password = pssword;
      
      try {
         initializeActiveMq();
      }
      catch (Exception e) {
         //show message saying user name and or password is wrong.
      }

      if( connectedToChatSession() ){
         loginDlg.setVisible(false);
         initGui();
         return;
      }
      loginDlg.clear();
   }
   
   @Override
   public void failLogin(){
      JOptionPane.showMessageDialog(frame, "Login Failed");
   }

   public void windowClosing(WindowEvent e) {
      e.getWindow().dispose();
   }

   public void windowClosed(WindowEvent e) {
      try{
         exit();
      } catch(Exception exc){}
   }

   public void windowActivated(WindowEvent e) {
   }

   public void windowDeactivated(WindowEvent e) {
   }

   public void windowDeiconified(WindowEvent e) {
   }

   public void windowIconified(WindowEvent e) {
   }

   public void windowOpened(WindowEvent e) {
   }

   
   /*
    * To be overridden by dervied classes to output senders message.
    */
   @Override
   protected void outputMessage (String sender, int type, String msgText) {
      
      scp.newMessage(sender, type, msgText);
   }

   /*
    * END INTERFACE MessageListener
    */

   /*
    * Disconnects from chat session. destroyChatSession() performs the JMS
    * cleanup.
    */
   @Override
   protected void doDisconnect() {
      super.doDisconnect();
      try {
      } catch (Exception e) {
         e.printStackTrace();
      }
      scp.setEnabled(false);
   }  
   
   /*
    * Create the application GUI.
    */
   private void initGui() {

      frame = new Frame(" Chat");
      frame.addWindowListener(this);
      MenuBar menubar = createMenuBar();
      frame.setMenuBar(menubar);

      scp = new ChatPanel();
      scp.setUserName(name);
      scp.setDestName(topicName);
      scp.setHostName(hostName);
      sendB = scp.getSendButton();
      sendB.addActionListener(this);

      frame.add(scp);
      frame.pack();
      frame.setVisible(true);

      scp.setEnabled(true);
   }

   /*
    * Create menubar for application.
    */
   private MenuBar createMenuBar() {
      MenuBar mb = new MenuBar();
      Menu chatMenu;

      chatMenu = (Menu) mb.add(new Menu("Connection Spec"));

      clearItem = (MenuItem) chatMenu.add(new MenuItem("Clear Messages"));
      exitItem = (MenuItem) chatMenu.add(new MenuItem("Log Out"));

      clearItem.addActionListener(this);
      exitItem.addActionListener(this);

      return (mb);
   }

   
   /*
    * To be overriden by subclasses. Takes message input by user and sends it to server
    */
   @Override
   protected String fetchInputMessage() throws Exception{
      String msg = scp.getMessage();
      scp.setMessage("");
      return msg;
   }
   
   protected void showTopic() {
      if( topicDlg == null ){
         topicDlg = new TopicDialog(frame);
         topicDlg.addWindowListener(this);
         joinB = topicDlg.getJoinButton();
         joinB.addActionListener(this);
      }
      topicDlg.setVisible(true);
   }
   
   private void showLogin(){
      if( loginDlg == null ){
         loginDlg = new LoginDialog(frame);
         loginDlg.addWindowListener(this);
         loginB = loginDlg.getLoginButton();
         loginB.addActionListener(this);
         createB = loginDlg.getCreateButton();
         createB.addActionListener(this);
      }
      loginDlg.setVisible(true);
   }

   /**
    * @param args
    *           Arguments passed via the command line. These are used to create
    *           the ConnectionFactory.
    * @throws Exception 
    */
   public static void main(String[] args) throws Exception {
      
      Chat sc = new Chat();
      sc.url = args[0];
      //sc.topicName = args[1];
      
      sc.showLogin();
      //sc.initGui();
      //sc.initializeActiveMq();
   }

}
