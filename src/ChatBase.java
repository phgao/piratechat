import java.net.InetAddress;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.Topic;

import org.apache.activemq.ActiveMQConnectionFactory;

public class ChatBase implements MessageListener {

   ActiveMQConnectionFactory connectionFactory;
   Connection connection;
   Session session;
   MessageProducer msgProducer;
   MessageConsumer msgConsumer;
   Topic topic;

   boolean connected = false;
   boolean isConsole = false;
   String name, password, url, hostName, topicName, outgoingMsgTypeString;
   int outgoingMsgType;

   ChatMessageCreator outgoingMsgCreator;
   ChatMessageCreator txtMsgCreator;

   /**
    * Chat constructor. Initializes the chat user name, topic, hostname.
    */
   public ChatBase() {
      name = System.getProperty("user.name", "johndoe");
      topicName = "defaulttopic";

      try {
         hostName = InetAddress.getLocalHost().getHostName();
      } catch (Exception e) {
         hostName = "localhost";
      }
   }

   /**
    * Returns a new chattextmessageCreator
    * 
    * @return ChatMessageCreator
    */
   public ChatMessageCreator getMessageCreator() {
      return new ChatTextMessageCreator();
   }

   /**
    * onMessage receives messages from the broker whenever a new message
    * has been sent to the broker.
    */
   public void onMessage(Message msg) {
      String sender, msgText;
      int type;
      
      ChatMessageCreator inboundMsgCreator;
      inboundMsgCreator = getMessageCreator();

      if (inboundMsgCreator == null) {
         errorMessage("Message received is not supported ! ");
         return;
      }

      /*
       * Need to fetch msg values in this order.
       */
      type = inboundMsgCreator.getChatMessageType(msg);
      sender = inboundMsgCreator.getChatMessageSender(msg);
      msgText = inboundMsgCreator.getChatMessageText(msg);

      if (type == ChatMessageTypes.BADTYPE) {
         errorMessage("Message received in wrong format ! ");
         return;
      }
      
      outputMessage (sender, type, msgText);
   }

   /*
    * To be overridden by dervied classes to output senders message.
    */
   protected void outputMessage (String sender, int type, String msgText) {}
   
   /*
    * Performs the actual chat connect. The createChatSession() method does the
    * real work here, creating: Connection Session Topic MessageConsumer
    * MessageProducer
    */
   protected void doConnect() {
      if (connectedToChatSession())
         return;

      outgoingMsgCreator = getMessageCreator();

      if (createChatSession(topicName) == false) {
         errorMessage("Unable to create Chat session.  "
               + "Please verify a broker is running");
         return;
      }
      setConnectedToChatSession(true);
   }

   /*
    * Disconnects from chat session. destroyChatSession() performs the JMS
    * cleanup.
    */
   protected void doDisconnect() {
      if (!connectedToChatSession())
         return;

      destroyChatSession();

      setConnectedToChatSession(false);
   }

   /*
    * These methods set/return a flag that indicates whether the application is
    * currently involved in a chat session.
    */
   protected void setConnectedToChatSession(boolean b) {
      connected = b;
   }

   protected boolean connectedToChatSession() {
      return (connected);
   }

   /*
    * Exit application. Does some cleanup if necessary.
    */
   protected void exit() {
      doDisconnect();
      System.exit(0);
   }

   /*
    * Send message using text that is currently in the ChatPanel object. The
    * text message is obtained via scp.getMessage()
    * 
    * An object of type ChatObjMessage is created containing the typed text. A
    * JMS ObjectMessage is used to encapsulate this ChatObjMessage object.
    */
   protected void sendNormalMessage() {
      Message msg;

      if (!connectedToChatSession()) {
         errorMessage("Cannot send message: Not connected to chat session!");

         return;
      }

      try {
         msg = outgoingMsgCreator.createChatMessage(session, name,
               ChatMessageTypes.NORMAL, fetchInputMessage());

         msgProducer.send(msg);
      } catch (Exception ex) {
         errorMessage("Caught exception while sending NORMAL message: " + ex);
      }
   }

   /*
    * To be overriden by subclasses. Takes message input by user and sends it to server
    */
   protected String fetchInputMessage() throws Exception{
      return null;
   }
   
   /*
    * Send a message to the chat session to inform people we just joined the
    * chat.
    */
   protected void sendJoinMessage() {
      Message msg;

      try {
         msg = outgoingMsgCreator.createChatMessage(session, name, ChatMessageTypes.JOIN, "Join chat " + session);

         msgProducer.send(msg);
      } catch (Exception ex) {
         errorMessage("Caught exception while sending JOIN message: " + ex);
      }
   }

   /*
    * Send a message to the chat session to inform people we are leaving the
    * chat.
    */
   protected void sendLeaveMessage() {
      Message msg;

      try {
         msg = outgoingMsgCreator.createChatMessage(session, name, ChatMessageTypes.LEAVE, "Leaving chat" + session);

         msgProducer.send(msg);
      } catch (Exception ex) {
         errorMessage("Caught exception while sending LEAVE message: " + ex);
      }
   }

   /*
    * Initialization for ActiveMQ conntection factory. This is simply creating the ConnectionFactory.
    */
   protected void initializeActiveMq() {
      try {
         connectionFactory = new ActiveMQConnectionFactory(name, password, url);
         doConnect();
         
      } catch (Exception e) {
         failLogin();
      }
   }
   
   protected void createAccount(){}

   /*
    * Overriden in subclasses( will vary based on UI or console)
    */
   protected void failLogin() {}

   /*
    * Create 'chat session'. This involves creating: Connection Session Topic
    * MessageConsumer MessageProducer
    */
   protected boolean createChatSession(String topicStr) {
      try {
         connection = connectionFactory.createConnection();
         session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

         topic = session.createTopic(topicStr);
         msgProducer = session.createProducer(topic);
         msgProducer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
         msgConsumer = session.createConsumer(topic);
         msgConsumer.setMessageListener(this);

         connection.start();

         sendJoinMessage();

         return true;

      } catch (Exception e) {
         failLogin();
         return false;
      }
   }

   /*
    * Destroy/close 'chat session'.
    */
   protected void destroyChatSession() {
      try {
         sendLeaveMessage();

         msgConsumer.close();
         msgProducer.close();
         session.close();
         connection.close();

         topic = null;
         msgConsumer = null;
         msgProducer = null;
         session = null;
         connection = null;

      } catch (Exception e) {
         errorMessage("Caught Exception: " + e);
      }

   }

   /*
    * Display error. Right now all we do is dump to stderr.
    */
   protected void errorMessage(String s) {
      System.err.println(s);
   }

}
