import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;

import javax.jms.*;

import org.apache.activemq.ActiveMQConnectionFactory;

public class ConsoleChat implements MessageListener {

   ActiveMQConnectionFactory connectionFactory;
   Connection connection;
   Session session;
   MessageProducer msgProducer;
   MessageConsumer msgConsumer;
   Topic topic;

   boolean connected = false;
   String name, hostName, topicName, outgoingMsgTypeString;
   int outgoingMsgType;

   ChatMessageCreator outgoingMsgCreator;
   ChatMessageCreator txtMsgCreator;

   /**
    * Chat constructor. Initializes the chat user name, topic, hostname.
    */
   public ConsoleChat() {
      name = System.getProperty("user.name", "johndoe");
      topicName = "defaulttopic";

      try {
         hostName = InetAddress.getLocalHost().getHostName();
      } catch (Exception e) {
         hostName = "localhost";
      }
   }

   public ChatMessageCreator getMessageCreator() {
      return new ChatTextMessageCreator();
   }

   public void onMessage(Message msg) {
      String sender, msgText;
      int type;
      ChatMessageCreator inboundMsgCreator;

      inboundMsgCreator = getMessageCreator();

      if (inboundMsgCreator == null) {
         errorMessage("Message received is not supported ! ");
         return;
      }

      /*
       * Need to fetch msg values in this order.
       */
      type = inboundMsgCreator.getChatMessageType(msg);
      sender = inboundMsgCreator.getChatMessageSender(msg);
      msgText = inboundMsgCreator.getChatMessageText(msg);

      if (type == ChatMessageTypes.BADTYPE) {
         errorMessage("Message received in wrong format ! ");
         return;
      }
      
      System.out.println(sender + ": " + msgText + "\n");
   }

   /*
    * Performs the actual chat connect. The createChatSession() method does the
    * real work here, creating: Connection Session Topic MessageConsumer
    * MessageProducer
    */
   private void doConnect() {
      if (connectedToChatSession())
         return;

      outgoingMsgCreator = getMessageCreator();

      if (createChatSession(topicName) == false) {
         errorMessage("Unable to create Chat session.  "
               + "Please verify a broker is running");
         return;
      }
      setConnectedToChatSession(true);
   }

   /*
    * Disconnects from chat session. destroyChatSession() performs the JMS
    * cleanup.
    */
   private void doDisconnect() {
      if (!connectedToChatSession())
         return;

      destroyChatSession();

      setConnectedToChatSession(false);
   }

   /*
    * These methods set/return a flag that indicates whether the application is
    * currently involved in a chat session.
    */
   private void setConnectedToChatSession(boolean b) {
      connected = b;
   }

   private boolean connectedToChatSession() {
      return (connected);
   }

   /*
    * Exit application. Does some cleanup if necessary.
    */
   private void exit() {
      doDisconnect();
      System.exit(0);
   }

   /*
    * Send message using text that is currently in the ChatPanel object. The
    * text message is obtained via scp.getMessage()
    * 
    * An object of type ChatObjMessage is created containing the typed text. A
    * JMS ObjectMessage is used to encapsulate this ChatObjMessage object.
    */
   private void sendNormalMessage() {
      Message msg;

      if (!connectedToChatSession()) {
         errorMessage("Cannot send message: Not connected to chat session!");

         return;
      }

      try {
         msg = outgoingMsgCreator.createChatMessage(session, name,
               ChatMessageTypes.NORMAL, fetchInputMessage());

         msgProducer.send(msg);
      } catch (Exception ex) {
         errorMessage("Caught exception while sending NORMAL message: " + ex);
      }
   }

   private String fetchInputMessage() throws Exception
   {
      // Read from command line
      BufferedReader commandLine = new java.io.BufferedReader(new InputStreamReader(System.in));
      
      //check to see if the user wishes to exit
      String val = commandLine.readLine();
      if( val.toLowerCase() == "exit"){
         commandLine.close();
         commandLine = null;
         exit();
      }
      return val;
   }
   
   /*
    * Send a message to the chat session to inform people we just joined the
    * chat.
    */
   private void sendJoinMessage() {
      Message msg;

      try {
         msg = outgoingMsgCreator.createChatMessage(session, name, ChatMessageTypes.JOIN, "Join chat " + session);

         msgProducer.send(msg);
      } catch (Exception ex) {
         errorMessage("Caught exception while sending JOIN message: " + ex);
      }
   }

   /*
    * Send a message to the chat session to inform people we are leaving the
    * chat.
    */
   private void sendLeaveMessage() {
      Message msg;

      try {
         msg = outgoingMsgCreator.createChatMessage(session, name, ChatMessageTypes.LEAVE, "Leaving chat" + session);

         msgProducer.send(msg);
      } catch (Exception ex) {
         errorMessage("Caught exception while sending LEAVE message: " + ex);
      }
   }

   /*
    * Initialization for ActiveMQ conntection factory. This is simply creating the ConnectionFactory.
    */
   private void initializeActiveMq(String args[]) {
      try {
         //String url = "failover:(tcp://localhost:6160)?maxReconnectDelay=5000";
         String url = args[0];
         name = args[1];
         topicName = args[2];
         connectionFactory = new ActiveMQConnectionFactory(url);
         doConnect();
         sendNormalMessage();
      } catch (Exception e) {
         errorMessage("Caught Exception: " + e);
      }
   }

   /*
    * Create 'chat session'. This involves creating: Connection Session Topic
    * MessageConsumer MessageProducer
    */
   private boolean createChatSession(String topicStr) {
      try {
         connection = connectionFactory.createConnection();
         session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

         topic = session.createTopic(topicStr);
         msgProducer = session.createProducer(topic);
         msgProducer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
         msgConsumer = session.createConsumer(topic);
         msgConsumer.setMessageListener(this);

         connection.start();

         sendJoinMessage();

         return true;

      } catch (Exception e) {
         errorMessage("Caught Exception: " + e);
         e.printStackTrace();
         return false;
      }
   }

   /*
    * Destroy/close 'chat session'.
    */
   private void destroyChatSession() {
      try {
         sendLeaveMessage();

         msgConsumer.close();
         msgProducer.close();
         session.close();
         connection.close();

         topic = null;
         msgConsumer = null;
         msgProducer = null;
         session = null;
         connection = null;

      } catch (Exception e) {
         errorMessage("Caught Exception: " + e);
      }

   }

   /*
    * Display error. Right now all we do is dump to stderr.
    */
   private void errorMessage(String s) {
      System.err.println(s);
   }

   /**
    * @param args
    *           Arguments passed via the command line. These are used to create
    *           the ConnectionFactory.
    */
   public static void main(String[] args) {
      ConsoleChat sc = new ConsoleChat();

      sc.initializeActiveMq(args);
   }

}
